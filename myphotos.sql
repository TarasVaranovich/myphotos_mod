CREATE DATABASE myphotos;
USE myphotos;
CREATE TABLE usr (id SMALLINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
				    name BLOB, 
				    password BLOB); /*65kB blob*/
CREATE TABLE photo (id SMALLINT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
					usrid SMALLINT NOT NULL,
					name VARCHAR(30), 
					content MEDIUMBLOB,
					format VARCHAR(10) ,
					description TEXT,
					originDate DATETIME,
					FOREIGN KEY (usrid) REFERENCES usr (id)); /*16 mB blob*/
CREATE TABLE document (id SMALLINT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
					usrid SMALLINT NOT NULL,
					name VARCHAR(30), 
					content MEDIUMBLOB,
					originDate DATETIME,
					FOREIGN KEY (usrid) REFERENCES usr (id)); /*16 mB blob*/					